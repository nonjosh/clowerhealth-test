"""PostgreSQL helper class"""
import sqlalchemy


class PgsqlHelper:
    def __init__(self, pgsql_dsn) -> None:
        # Connect to the PostgreSQL database
        try:
            self.engine = sqlalchemy.create_engine(pgsql_dsn)
        except sqlalchemy.exc.OperationalError as err:
            print(f"Error: {err}")
            print("I am unable to connect to the database")

    def get_version(self):
        """Get the version of the PostgreSQL database"""
        return self.engine.execute("SELECT version();").fetchone()[0]

    def close(self) -> None:
        """Close the factory of the PostgresQL connection"""
        self.engine.dispose()
