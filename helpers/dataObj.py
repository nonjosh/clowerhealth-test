"""Data object class"""
import os
import pandas as pd


class DataObj:
    """Data object class"""
    def __init__(self, data_txt_filepath: str, spec_obj):

        # Check if the files exist
        if not os.path.isfile(data_txt_filepath):
            raise FileNotFoundError(f"Data file {data_txt_filepath} not found")

        # Check if the file is a txt file
        if not data_txt_filepath.endswith(".txt"):
            raise ValueError(f"Data file {data_txt_filepath} is not a txt file")

        self.data_txt_filepath = data_txt_filepath
        self.spec_obj = spec_obj

    def data_txt_to_df(self):
        """
        Iterate over the data txt file and return a DataFrame

        Parameters
        ----------
        txt_filepath : str
            Path to the data file
        spec_csv_df : pd.DataFrame
            DataFrame with the specification

        Returns
        -------
        pd.DataFrame
            DataFrame with the data
        """

        data_df = self.spec_obj.get_empty_spec_df()

        with open(self.data_txt_filepath, "r", encoding="utf8") as file_obj:
            lines = file_obj.readlines()
            for row_str in lines:
                row_dict = self.spec_obj.get_data_row_dict(
                    row_str=row_str.rstrip(),
                )
                row_df = pd.DataFrame(row_dict, index=[0])
                data_df = pd.concat([data_df, row_df], ignore_index=True)

        return data_df
