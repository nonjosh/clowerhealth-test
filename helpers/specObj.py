"""Specification object class and functions"""
import os
import pandas as pd


def get_dtype(format_str: str):
    """Return the dtype for the given format

    Parameters
    ----------
    format_str : str
        Format of the data

    Returns
    -------
    str
        dtype for the given format
    """

    if format_str == "INTEGER":
        return int
    elif format_str == "FLOAT":
        return float
    elif format_str == "TEXT":
        return str
    elif format_str == "DATE":
        return "datetime64[D]"
    elif format_str == "DATETIME":
        return "datetime64[s]"
    elif format_str == "BOOLEAN":
        return bool
    else:
        raise ValueError(f"Unknown format: {format_str}")


def get_format_data(input_str: str, format_str: str):
    """Return the formatted data from the input_str in the format_str

    Parameters
    ----------
    input_str : str
        String with the data
    format_str : str
        Format of the data

    Returns
    -------
    var
        Data with the proper format

    Raises
    ------
    ValueError
        If the format is not valid
    """

    if format_str == "INTEGER":
        return int(input_str)
    elif format_str == "FLOAT":
        return float(input_str)
    elif format_str == "BOOLEAN":
        return bool(int(input_str))
    elif format_str == "DATE":
        return pd.to_datetime(input_str)
    elif format_str == "DATETIME":
        return pd.to_datetime(input_str)
    elif format_str == "TEXT":
        return input_str.strip()
    raise ValueError(f"Unknown format: {format_str}")


class SpecObj:
    """Specification object class"""

    def __init__(self, spec_csv_filepath, verbose=True):

        # Check if the files exist
        if not os.path.isfile(spec_csv_filepath):
            raise FileNotFoundError(f"Specification file {spec_csv_filepath} not found")

        # Check if the file is a csv file
        if not spec_csv_filepath.endswith(".csv"):
            raise ValueError(
                f"Specification file {spec_csv_filepath} is not a csv file"
            )

        self.spec_csv_filepath = spec_csv_filepath
        self.spec_csv_df = pd.read_csv(spec_csv_filepath)
        self.total_width = sum(self.spec_csv_df["width"])
        self.spec_tablename = os.path.basename(spec_csv_filepath)[:-4]

        if verbose:
            # Print number of columns find in the specification file
            print(f"Specifications of {len(self.spec_csv_df)} columns are found")

    def get_empty_spec_df(self):
        """Return an empty DataFrame of the specs from the spec_csv_df

        Parameters
        ----------
        spec_csv_df : pd.DataFrame
            DataFrame with the specs

        Returns
        -------
        pd.DataFrame
            empty DataFrame with the specs columns
        """

        spec_dict = {}
        for _, row in self.spec_csv_df.iterrows():
            spec_dict[row["column name"]] = pd.Series(dtype=get_dtype(row["datatype"]))
        return pd.DataFrame(spec_dict)

    def get_data_row_dict(self, row_str: str):
        """Return a dictionary with the data from a row of string

        Parameters
        ----------
        row_str : str
            String with the data
        spec_csv_df : pd.DataFrame
            DataFrame with the specification

        Returns
        -------
        dict
            Dictionary with the data

        Raises
        ------
        ValueError
            If the row length is not valid
        """

        result_dict = {}
        # Check if the row length is correct
        if len(row_str) != self.total_width:
            print(
                f"Row length is not valid, {self.total_width} is expected but {len(row_str)} is found"
            )
            raise ValueError(f"Row length is not correct: {row_str}")

        for _, row in self.spec_csv_df.iterrows():
            # Get column name and width
            column_name = row["column name"]
            column_width = row["width"]

            # Get the data from the row
            result_dict[column_name] = row_str[:column_width]

            # Change the format to the correct type
            result_dict[column_name] = get_format_data(
                input_str=result_dict[column_name], format_str=row["datatype"]
            )

            # Remove the data from the row
            row_str = row_str[column_width:]
        return result_dict
