# Engineering Exercise – Clover Health

Import data files into PostgreSQL

## Setup

Decide how to set the connection string for PostgreSQL.

The program should try to find the connection string value in the following order:

1. argument `--pgsqldsn`
2. environment variable `PGSQL_DSN`
3. default connection string `postgres://postgres:postgres@localhost:5432/postgres`

create `.env` file in the root directory of the project with the following variable:

  - `PGSQL_DSN`

`.env` example:

```txt
PGSQL_DSN=postgresql://postgres:postgres@db:5432/postgres
```

## Usage

```txt
usage: main.py [-h] [--spec SPEC_CSV_FILEPATH] [--data DATA_TXT_FILEPATH] [--pgsqldsn PGSQL_DSN]

Import data files into PostgreSQL

options:
  -h, --help            show this help message and exit
  --spec SPEC_CSV_FILEPATH
                        Specifiy specification file (default: specs/fileformat1.csv)
  --data DATA_TXT_FILEPATH
                        Specifiy data file (default: data/fileformat1_2020-10-15.txt)
  --pgsqldsn PGSQL_DSN  Specifiy PostgreSQL DSN (default: postgresql://postgres:postgres@db:5432/postgres)
```

example:

```sh
python main.py --spec_csv_filepath=<path_to_spec_csv_file> --data_txt_filepath=<path_to_data_txt_file>
```

### Supported Data Formats

- TEXT
- INTEGER
- FLOAT
- DATE
- DATETIME
- BOOLEAN

## Unit Tests

```sh
python -m unittest -v
```

## Assumption

- no empty line in data txt file

## Possible extensions

- add support for multiple data/specifiction files
- add columns in tables (e.g. index, file datetime, insert timestamp, etc.)
- add primary key and check duplicated records
- save specifictions in database
- allow update of columns in existing tables
- dockerize the program?
