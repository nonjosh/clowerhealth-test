import unittest
from helpers import PgsqlHelper

DEFAULT_PGSQL_DSN = "postgresql://postgres:postgres@db:5432/postgres"


class TestPgsql(unittest.TestCase):
    def test_query(self):
        pgsql_helper = PgsqlHelper(pgsql_dsn=DEFAULT_PGSQL_DSN)
        # pgsql_helper.connect()
        self.assertEqual(
            pgsql_helper.get_version(),
            "PostgreSQL 14.4 (Debian 14.4-1.pgdg110+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit",
        )
        pgsql_helper.close()
