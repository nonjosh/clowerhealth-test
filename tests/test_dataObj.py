import unittest
from helpers import DataObj, SpecObj


class TestDataObj(unittest.TestCase):
    def test_data_obj(self):

        spec_obj = SpecObj(
            spec_csv_filepath="tests/data/sampleformat1.csv", verbose=False
        )

        self.assertRaises(
            FileNotFoundError,
            DataObj,
            data_txt_filepath="tests/data/missingfile.txt",
            spec_obj=spec_obj,
        )
        self.assertRaises(
            ValueError,
            DataObj,
            data_txt_filepath="tests/data/sampleformat1.csv",
            spec_obj=spec_obj,
        )
        data_obj = DataObj(
            data_txt_filepath="tests/data/sampleformat1_2022-06-22.txt",
            spec_obj=spec_obj,
        )
        self.assertEqual(data_obj.data_txt_to_df().shape, (2, 5))
