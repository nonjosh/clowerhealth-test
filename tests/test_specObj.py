import unittest
from datetime import datetime
import pandas as pd
from helpers.specObj import SpecObj, get_dtype, get_format_data


class TestSpecObj(unittest.TestCase):
    def test_get_dtype(self):
        self.assertEqual(get_dtype("INTEGER"), int)
        self.assertEqual(get_dtype("FLOAT"), float)
        self.assertEqual(get_dtype("BOOLEAN"), bool)
        self.assertEqual(get_dtype("DATE"), "datetime64[D]")
        self.assertEqual(get_dtype("DATETIME"), "datetime64[s]")
        self.assertEqual(get_dtype("TEXT"), str)

    def test_get_format_data(self):
        self.assertEqual(
            get_format_data(input_str="Diabetes  ", format_str="TEXT"), "Diabetes"
        )
        self.assertEqual(get_format_data(input_str="1", format_str="INTEGER"), 1)
        self.assertEqual(get_format_data(input_str="-14", format_str="INTEGER"), -14)
        self.assertEqual(get_format_data(input_str="1.5", format_str="FLOAT"), 1.5)
        self.assertEqual(get_format_data(input_str="-1.5", format_str="FLOAT"), -1.5)
        self.assertEqual(get_format_data(input_str="1", format_str="BOOLEAN"), True)
        self.assertEqual(get_format_data(input_str="0", format_str="BOOLEAN"), False)
        self.assertEqual(get_format_data(input_str=" 1 ", format_str="BOOLEAN"), True)
        self.assertEqual(get_format_data(input_str="   0", format_str="BOOLEAN"), False)
        self.assertEqual(
            get_format_data(input_str="2020-01-01", format_str="DATE"),
            datetime(2020, 1, 1),
        )
        self.assertEqual(
            get_format_data(input_str="2020-01-02T03:04:05", format_str="DATETIME"),
            datetime(2020, 1, 2, 3, 4, 5),
        )
        self.assertEqual(
            get_format_data(input_str="2020-01-02 3:4:5", format_str="DATE"),
            datetime(2020, 1, 2, 3, 4, 5),
        )
        self.assertEqual(
            get_format_data(input_str="2009/8/7 6:5:4", format_str="DATE"),
            datetime(2009, 8, 7, 6, 5, 4),
        )
        self.assertRaises(
            ValueError, get_format_data, input_str="1+1", format_str="MATH"
        )
        self.assertRaises(ValueError, get_format_data, input_str="1", format_str="DATE")

    def test_spec_obj(self):
        self.assertRaises(
            FileNotFoundError,
            SpecObj,
            spec_csv_filepath="tests/data/sampleformat999.csv",
            verbose=False,
        )
        self.assertRaises(
            ValueError,
            SpecObj,
            spec_csv_filepath="tests/data/sampleformat1_2022-06-22.txt",
            verbose=False,
        )

        spec_obj = SpecObj(
            spec_csv_filepath="tests/data/sampleformat1.csv", verbose=False
        )

        sample_empty_df = pd.DataFrame(
            {
                "name": pd.Series(dtype=str),
                "age": pd.Series(dtype=int),
                "height": pd.Series(dtype=float),
                "valid": pd.Series(dtype=bool),
                "birthday": pd.Series(dtype="datetime64[D]"),
            }
        )
        self.assertEqual(True, spec_obj.get_empty_spec_df().equals(sample_empty_df))

        sample_row_str = "Diabetes  18 175.211993-01-02"
        sample_row_dict = {
            "name": "Diabetes",
            "age": 18,
            "height": 175.2,
            "valid": True,
            "birthday": datetime(1993, 1, 2),
        }
        self.assertEqual(
            spec_obj.get_data_row_dict(row_str=sample_row_str), sample_row_dict
        )
        sample_row_str = "Asthma    30   21101994-03-04"
        sample_row_dict = {
            "name": "Asthma",
            "age": 30,
            "height": 211,
            "valid": False,
            "birthday": datetime(1994, 3, 4),
        }
        self.assertEqual(
            spec_obj.get_data_row_dict(row_str=sample_row_str), sample_row_dict
        )
