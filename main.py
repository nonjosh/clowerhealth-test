"""Import data files into PostgreSQL
Usage:
    python main.py --spec_csv_filepath=<spec_csv_filepath> --data_txt_filepath=<data_txt_filepath>
"""
import os
import argparse
from helpers import SpecObj, DataObj, PgsqlHelper

DEFAULT_PGSQL_DSN = "postgresql://postgres:postgres@db:5432/postgres"

DEFAULT_SPEC_CSV_FILE = "specs/fileformat1.csv"
DEFAULT_DATA_TXT_FILE = "data/fileformat1_2020-10-15.txt"


def main():
    """main function"""

    # Set arguments for specifying the data file and the spec file
    parser = argparse.ArgumentParser(description="Import data files into PostgreSQL")
    parser.add_argument(
        "--spec",
        type=str,
        default=DEFAULT_SPEC_CSV_FILE,
        dest="spec_csv_filepath",
        help=f"Specifiy specification file (default: {DEFAULT_SPEC_CSV_FILE})",
    )
    parser.add_argument(
        "--data",
        type=str,
        default=DEFAULT_DATA_TXT_FILE,
        dest="data_txt_filepath",
        help=f"Specifiy data file (default: {DEFAULT_DATA_TXT_FILE})",
    )
    parser.add_argument(
        "--pgsqldsn",
        type=str,
        dest="pgsql_dsn",
        help=f"Optional: Specifiy PostgreSQL DSN (default: {DEFAULT_PGSQL_DSN})",
    )
    args = parser.parse_args()

    # Set filepaths
    spec_csv_filepath = args.spec_csv_filepath
    data_txt_filepath = args.data_txt_filepath

    # Set the PostgreSQL DSN
    if args.pgsql_dsn:
        print(f"Using PostgreSQL DSN: {args.pgsql_dsn}")
        pgsql_dsn = args.pgsql_dsn
    elif "PGSQL_DSN" in os.environ:
        print("Using PGSQL_DSN from environment")
        pgsql_dsn = os.environ["PGSQL_DSN"]
    else:
        print(f"Using default PostgreSQL DSN: {DEFAULT_PGSQL_DSN}")
        pgsql_dsn = DEFAULT_PGSQL_DSN

    # Create specification object
    spec_obj = SpecObj(spec_csv_filepath=spec_csv_filepath)

    # Create data object
    data_obj = DataObj(data_txt_filepath=data_txt_filepath, spec_obj=spec_obj)

    # Connect to the PostgreSQL database
    pgsql_helper = PgsqlHelper(pgsql_dsn=pgsql_dsn)

    # Create a table in PGSQL with the specification if not exists
    # Create an empty DataFrame with the specification
    spec_df = spec_obj.get_empty_spec_df()
    # Create a table in PGSQL with the specification if not exists
    spec_df.to_sql(
        spec_obj.spec_tablename,
        con=pgsql_helper.engine,
        if_exists="append",
        index=False,
    )

    # Read the data file
    data_df = data_obj.data_txt_to_df()

    # Insert the data into the PostgreSQL table
    data_df.to_sql(
        name=spec_obj.spec_tablename,
        con=pgsql_helper.engine,
        if_exists="append",
        index=False,
    )
    print(f"Inserted {len(data_df)} rows into the table {spec_obj.spec_tablename}")

    # Close the factory of the PostgresQL connection
    pgsql_helper.close()


if __name__ == "__main__":
    main()
